variable "aws_access_key" {
  type = string
}

variable "aws_secret_key" {
  type = string
}

variable "test_gitlab_ci_tfvar" {
  type = string
}
